package com.citi.training.stocks;

import javax.persistence.GeneratedValue;

@Entity
public class Stock {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String ticker;
	private String companyName;
	
	public Stock() {
		
	}
	
	public Stock(long id, String ticker, String companyName) {
		this.id = id;
		this.ticker = ticker;
		this.companyName = companyName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getticker() {
		return ticker;
	}

	public void setticker(String ticker) {
		this.ticker = ticker;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Override
	public String toString() {
		return "Stock [id=" + id + ", ticker=" + ticker + ", companyName=" + companyName + "]";
	}
	
	

}
