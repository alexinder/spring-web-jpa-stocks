package com.citi.training.stocks;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.citi.training.products.repo.ProductRepository;

public class StockController {
	
	@Autowired
    StockDao stockThing;
	
	public Iterable<Stock> findAll() {
		// This method returns a list of all stocks
		return stockThing.findAll();	
	}
	
	public Stock findById(@PathVariable long id) {
		// This method finds a stock object by the id of the stock passed in as an argument
		return stockThing.findById(id).get(id);
	}
	
	public void create (@RequestBody Stock stock) {
		// This method creates a new stock given the stock object
		stockThing.save(stock)
	}
	
	public ResponseBody void deleteById(@PathVariable long id)  {
		stockThing.deleteById(id);	
	}

}
