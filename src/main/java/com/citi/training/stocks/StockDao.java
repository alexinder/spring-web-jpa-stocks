package com.citi.training.stocks;

import org.springframework.data.repository.CrudRepository;

public interface StockDao extends CrudRepository<Stock, Long> {

}
